import re

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Length, ValidationError

def email_check(form, field):
    msg="поле должно содержать корректный адрес электронной почты (example@example.com)"
    patern1=re.compile('^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$')
    if not patern1.search(field.data):
        raise ValidationError(msg)

def password_check(form, field):
    # msg="должен содержать латинские сивмолы и цифры"
    # patern1=re.compile('\w+') # [a-zA-Z0-9]+
    msg="поле должно содержать латинские сивмолы и цифры"
    patern1=re.compile('[a-zA-Z]+')
    patern2=re.compile('\d+')
    if (not patern1.search(field.data) or
        not patern2.search(field.data)):
        raise ValidationError(msg)

# Форма регистрации
class RegistrationForm(FlaskForm):
    email=StringField("Электропочта", 
            validators=[DataRequired(message="поле должно быть не пустым"), 
                        Length(min=4, max=32, message="не менее 4 и не боле 32 символов"),
                        email_check])

    password=PasswordField("Пароль", 
            validators=[DataRequired(message="поле должно быть не пустым"),
                        Length(min=8, message="не менее 8 символов"),
                        password_check])
# Форма входа
class LoginForm(FlaskForm):
    email=StringField("Электропочта", 
            validators=[DataRequired(message="поле должно быть не пустым")])
    password=PasswordField("Пароль", 
            validators=[DataRequired(message="поле должно быть не пустым")])

# Форма заказа
class OrderForm(FlaskForm):
    name=StringField("Ваше имя", 
            validators=[DataRequired(message="поле должно быть не пустым")])
    address=StringField("Адрес", 
            validators=[DataRequired(message="поле должно быть не пустым")])
    email=StringField("Электропочта", 
            validators=[DataRequired(message="поле должно быть не пустым"), 
                        Length(min=4, max=32, message="не менее 4 и не боле 32 символов"),
                        email_check])
    phone=StringField("Телефон", 
            validators=[DataRequired(message="поле должно быть не пустым")])
