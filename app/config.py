class Config:
    # General Config
    # SECRET_KEY=os.environ.get('SECRET_KEY')
    # FLASK_APP=os.environ.get('FLASK_APP')
    # FLASK_ENV=os.environ.get('FLASK_ENV')
    # FLASK_DEBUG=os.environ.get('FLASK_DEBUG')
    SECRET_KEY= "secret_key"
    DEBUG=True


    # Database
    # SQLALCHEMY_DATABASE_URI=os.environ.get('SQLALCHEMY_DATABASE_URI')
    # SQLALCHEMY_TRACK_MODIFICATIONS=os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')

    # app.config["SQLALCHEMY_DATABASE_URI"]="sqlite:///sqlite3.db" # База в файле 

    # SQLALCHEMY_DATABASE_URI="sqlite:///:memory:" # База в памяти
    # SQLALCHEMY_DATABASE_URI="sqlite:///sqlite3.db" # База в памяти
    SQLALCHEMY_DATABASE_URI="sqlite:///:memory:"
    SQLALCHEMY_TRACK_MODIFICATIONS=False
