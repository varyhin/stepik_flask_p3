from config import Config
from models import db, User, Order, Item, Category
from utils import init_load_db

from flask import Flask

# app=Flask(__name__, static_folder='static')
app=Flask(__name__)
app.config.from_object(Config)
app.app_context().push()

db.init_app(app)
db.create_all()
db.session.commit()
init_load_db()

from views import *

# https://stepik.org/lesson/287833/step/2?unit=290568
# https://stepik.org/lesson/287837/step/13?unit=269205
# https://stepik.org/lesson/287838/step/3?unit=269204

# https://flask.palletsprojects.com/en/1.1.x/quickstart/#sessions




