from main import app
from models import db, User, Order, Item, Category
from forms import LoginForm, RegistrationForm, OrderForm
from utils import get_tot_cart_price

from flask import render_template, request, flash, redirect, send_from_directory, session
from hashlib import sha256
import os
import uuid
from datetime import datetime

@app.route('/favicon.ico')
def favicon():
    # return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')
    return send_from_directory(app.static_folder, "favicon.ico")

@app.route("/")
def render_main():

    if not session.get("uid"):
        session["uid"]=str(uuid.uuid4())
        session["is_auth"]=False
        session["cart_items"]=[]
        session["tot_cart_items"]=0
        session["tot_cart_priсe"]=0
        
    # Отладка
    # flash(session.get("uid"), category='notice')
    # flash(session.get("is_auth"), category='notice')
    # flash(session.get("user_uid"), category='notice')
    # flash(session.get("email"), category='notice')

    return render_template("home.html", 
        c_all=db.session.query(Category).all(),
        i_all=db.session.query(Item).all(),
        tot_cart_items = session.get("tot_cart_items"),
        tot_cart_priсe = session.get("tot_cart_priсe")
        )

@app.route("/register/", methods=["GET", "POST"])
def render_register():

    form=RegistrationForm()

    if request.method == "POST":
        if form.validate_on_submit():

            # Если WTForms то form.email.data
            # Если просто формы в шаблонах то request.form.get("email")

            email=form.email.data
            password=form.password.data
            password_hash=sha256((password + email).encode()).hexdigest()

            user_data=db.session.query(User).filter_by(email=email).first()
            
            if not user_data:
                user = User(email=email, password=password_hash)
                db.session.add(user)
                db.session.commit()
                return redirect("/login/")
            else:
                flash("Пользователь c такой электронной почтой уже существует", category='notice')
                return redirect("/login/")
                
        else:
            flash("Форма не прошла валидацию", category='notice')
            return redirect("/register/")

    return render_template("register.html", form=form)

@app.route("/login/", methods=["GET", "POST"])
def render_login():

    form=LoginForm()

    if request.method == "POST":
        if form.validate_on_submit():

            email=form.email.data
            password=form.password.data
            password_hash=sha256((password + email).encode()).hexdigest()
            
            # для множеств
            # user_data=db.session.query(User).filter(User.email == email).first() 

            # для одиночного
            user_data=db.session.query(User).filter_by(email=email).first()

            # return "Почта: {} Пароль: {}".format(user_data.email, user_data.password)

            if not user_data:
                flash("Пользователь c такой электронной почтой не существует", category='notice')
                return redirect("/login/")
            
            if user_data and password_hash == user_data.password:
                # Устанавливаем в сессии признак, что пользователь аутентифицирован
                session["is_auth"]=True
                session["email"]=user_data.email
                session["user_uid"]=user_data.uid
                return redirect("/")
            else:
                flash("Введен неправильный пароль", category='notice')
                return redirect("/login/")

        flash("Форма не прошла валидацию", category='notice')
        return redirect("/login/")

    return render_template("login.html", form=form)

@app.route("/logout/")
def render_logout():
    
    session["is_auth"]=False

    return redirect("/")

@app.route("/cart/add/<int:item_uid>")
def render_cart_add(item_uid):

    items_in_cart=session.get("cart", [])
    items_in_cart.append(item_uid)
    session['cart']=items_in_cart
    
    session["tot_cart_items"] = len(session.get("cart", []))
    session["tot_cart_priсe"] = get_tot_cart_price(session.get("cart", []))

    # Как это делать без редиректа или так нормально!?!?!?!
    return redirect("/")

@app.route("/cart/del/<int:item_uid>")
def render_cart_del(item_uid):

    items_in_cart=session.get("cart", [])
    items_in_cart.remove(item_uid)
    session['cart']=items_in_cart
    
    session["tot_cart_items"] = len(session.get("cart", []))
    session["tot_cart_priсe"] = get_tot_cart_price(session.get("cart", []))

    return redirect("/cart/")

@app.route("/cart/")
def render_cart():

    form=OrderForm()

    cart_items=session.get("cart", [])
    # db.session.query(Item).group_by(Item.id).with_entities(Item.id).all()
    cart_items_data=db.session.query(Item.uid, Item.title, Item.price).filter(Item.uid.in_(cart_items)).all()

    cnt_cart_items = dict([[x,cart_items.count(x)] for x in set(cart_items)])
    tot_cart_items=session.get("tot_cart_items")
    tot_cart_priсe=session.get("tot_cart_priсe")

    if session.get("email"):
        user_email=session.get("email")
    else:
        user_email=""

    return render_template("cart.html",
        form=form,
        cart_items_data=cart_items_data,
        cnt_cart_items=cnt_cart_items,
        tot_cart_items=tot_cart_items,
        tot_cart_priсe=tot_cart_priсe,
        user_email=user_email
        )

@app.route("/order/", methods=["GET", "POST"])
def render_order():

    form=OrderForm()

    if request.method == "POST" and form.validate_on_submit():
        name=form.name.data
        address=form.address.data
        email=form.email.data
        phone=form.phone.data

        cart_items=str(session.get("cart", []))
        cart_items_data=db.session.query(Item.uid, Item.title, Item.price).filter(Item.uid.in_(cart_items)).all()
        tot_cart_priсe=session.get("tot_cart_priсe")

        order_date=datetime.today().strftime("%Y.%m.%d %H:%M")
        status = "done"

        order = Order(name=name, address=address, email=email, phone=phone, price=tot_cart_priсe, status=status, items=cart_items, dt=order_date)

        db.session.add(order)
        db.session.commit()

        # Очищаю корзину
        # session.pop("cart")
        # session["cart"] = []
        items_in_cart=[]
        session['cart']=items_in_cart
        session["tot_cart_items"] = len(session.get("cart", []))
        session["tot_cart_priсe"] = get_tot_cart_price(session.get("cart", []))        

        return redirect("/order/")

    return render_template("order.html")

@app.route("/account/")
def render_account():

    if not session["is_auth"]:
        return redirect("/login/")

    email=session.get("email")
    all_user_orders=db.session.query(Order).filter(Order.email == email).all()
    all_items=db.session.query(Item).all()

    return render_template("account.html",
        all_user_orders=all_user_orders,
        all_items=all_items
        )















