from flask_sqlalchemy import SQLAlchemy

db=SQLAlchemy()

class User(db.Model):
    __tablename__="users"
    uid=db.Column(db.Integer, primary_key=True)
    email=db.Column(db.String)
    password=db.Column(db.String)
    user_orders=db.Column(db.String)

class Order(db.Model):
    __tablename__="orders"
    uid=db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    email = db.Column(db.String)
    phone = db.Column(db.String)
    price=db.Column(db.Integer)
    status=db.Column(db.String)
    items=db.Column(db.String)
    dt=db.Column(db.String)

class Item(db.Model):
    __tablename__="items"
    uid=db.Column(db.Integer, primary_key=True)
    title=db.Column(db.String)
    price=db.Column(db.Integer)
    description=db.Column(db.String)
    picture=db.Column(db.String)
    category=db.Column(db.String)

class Category(db.Model):
    __tablename__="category"
    uid=db.Column(db.Integer, primary_key=True)
    title=db.Column(db.String)
