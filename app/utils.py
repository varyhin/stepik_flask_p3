import csv

from models import db, User, Order, Item, Category

def init_load_db():
    # Наполнение базы из файлов csv
    with open("./static/data/meals.csv","r") as csv_file: 
        csv_reader=csv.DictReader(csv_file)
        for line in csv_reader:
            item=Item(
                uid=line["id"], 
                title=line["title"], 
                price=line["price"], 
                description=line["description"], 
                picture=line["picture"], 
                category=line["category_id"]
                )
            db.session.add(item)

    with open("./static/data/categories.csv","r") as csv_file: 
        csv_reader=csv.DictReader(csv_file)
        for line in csv_reader:
            cat=Category(
                uid=line["id"],
                title=line["title"]
                )
            db.session.add(cat)

    db.session.commit()

def get_tot_cart_price(items):
    prices=dict(db.session.query(Item.uid, Item.price).filter(Item.uid.in_(items)).all())
    tot_price = 0
    for item in items:
        tot_price += prices[item]
    return tot_price
